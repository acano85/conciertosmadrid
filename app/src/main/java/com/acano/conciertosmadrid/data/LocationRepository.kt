package com.acano.conciertosmadrid.data

import android.content.Context
import com.google.android.gms.maps.model.LatLng

class LocationRepository (private val localLocationRepository: PreferencesSource, private val remoteLocationRepository: PreferencesRemoteSource) {


    suspend fun getLocation(): LatLng? {
            val location = remoteLocationRepository.getLocationFromRemote()
            if (location != null) {
                localLocationRepository.saveLocation(location)
            }
        return localLocationRepository.getLocation()
    }

    interface PreferencesSource {
        suspend fun saveLocation(location: LatLng)
        suspend fun getLocation(): LatLng?
    }
    interface PreferencesRemoteSource {
        suspend fun getLocationFromRemote(): LatLng?
    }
}
