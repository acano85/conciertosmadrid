package com.acano.conciertosmadrid.data

import com.acano.conciertosmadrid.domain.Place

class PlacesRepository (private val localPlacesRepository: LocalDataSource, private val remotePlacesRepository: RemoteDateSource){


   suspend fun getPlaces():List<Place>{

     if(localPlacesRepository.getPlaces().isNullOrEmpty()){
         val places = remotePlacesRepository.getPlaces()
         if(places.isNotEmpty())
         localPlacesRepository.savePlaces(places)
     }
     return localPlacesRepository.getPlaces()
 }

}

interface LocalDataSource{
    suspend fun isEmpty():Boolean
    suspend fun savePlaces(places:List<Place>)
    suspend fun getPlaces():List<Place>

}
interface RemoteDateSource{
   suspend fun getPlaces(): List<Place>
}
