package com.acano.conciertosmadrid.mvvm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.acano.conciertosmadrid.domain.Place
import com.acano.conciertosmadrid.storeitem.StoreItem
import com.acano.conciertosmadrid.usecases.GetLocationUseCase
import com.acano.conciertosmadrid.usecases.GetPlacesUseCase
import com.antonioleiva.mymovies.ui.common.ScopedViewModel
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

@Suppress("UNCHECKED_CAST")
class MainViewModel(private val placesUseCase: GetPlacesUseCase, private val locationUseCase: GetLocationUseCase): ScopedViewModel(){

    private val _model= MutableLiveData<UiModel>()
    val model:LiveData<UiModel>
        get(){
            if(_model.value==null){
                getLoc()
                getData()
            }
                return _model
            }

    sealed class UiModel{
        class showError(val error:String):UiModel()
        class getPlacesInfo(val places: List<StoreItem>):UiModel()
        class getLocationInfo(val latLng: LatLng?):UiModel()

    }

    init {
        initScope()
    }


    private fun getData() {
        GlobalScope.launch ( Dispatchers.Main) {
            val placesInfo=  async ( Dispatchers.IO) { MainViewModel.UiModel.getPlacesInfo(mapToStoreItems(placesUseCase.invoke()))}
            _model.value= placesInfo.await()
        }
    }

    fun getLoc() {
        GlobalScope.launch ( Dispatchers.Main) {
            val location=  async ( Dispatchers.IO) { MainViewModel.UiModel.getLocationInfo(locationUseCase.getLocation())}
            _model.value= location.await()
        }
    }
    override fun onCleared() {
        destroyScope()
        super.onCleared()
    }

    private fun mapToStoreItems(concerts: List<Place>): List<StoreItem> =
        concerts.map {
            StoreItem(
                Place(
                0,
                it.id,
                it.title,
                it.relation,
                it.lat,
                it.lon
            ))
        }


class MainViewModelFactory(val usecase: GetPlacesUseCase, val locations:GetLocationUseCase) :ViewModelProvider.Factory{

    override fun <T : ViewModel?> create(modelClass: Class<T>): T = MainViewModel(usecase, locations) as T
}
}