package com.acano.conciertosmadrid.usecases

import com.acano.conciertosmadrid.data.LocationRepository
import com.acano.conciertosmadrid.data.PlacesRepository
import com.acano.conciertosmadrid.domain.Place
import com.google.android.gms.maps.model.LatLng

class GetLocationUseCase(val locationRepository: LocationRepository) {
   suspend fun getLocation():LatLng? = locationRepository.getLocation()
}