package com.acano.conciertosmadrid.usecases

import com.acano.conciertosmadrid.data.PlacesRepository
import com.acano.conciertosmadrid.domain.Place

class GetPlacesUseCase(val placesRepository: PlacesRepository) {
    suspend fun invoke():List<Place> = placesRepository.getPlaces()
}