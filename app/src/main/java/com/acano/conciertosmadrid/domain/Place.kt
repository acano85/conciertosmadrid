package com.acano.conciertosmadrid.domain

data class Place(
    var uid:Int,
    var id:String,
    var title:String="",
    var relation:String="",
    var lat:Double?=0.0,
    var lon:Double?=0.0
)

