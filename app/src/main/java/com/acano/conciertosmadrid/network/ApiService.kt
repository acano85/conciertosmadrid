package com.acano.conciertosmadrid.network

import com.acano.conciertosmadrid.network.model.Concerts
import retrofit2.Call
import retrofit2.http.GET


const val BASE_URL:String="https://datos.madrid.es"
const val ConcertsEnpoint="/egob/catalogo/208862-7650180-ocio_salas.json?latitud=0&longitud=0&distancia=0"
interface ApiService {
    @GET(ConcertsEnpoint)
    fun getConcerts(): Call <Concerts>
}

