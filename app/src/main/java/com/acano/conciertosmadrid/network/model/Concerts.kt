package com.acano.conciertosmadrid.network.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable
@Parcelize
data class Concerts(
    @SerializedName("@graph")
    var places: List<Place>
):Parcelable


/*
"id": "417",
"name": "417 - Av. Duque de Ávila / Jardim Arco Do Cego",
"x": -9.142703,
"y": 38.735352,
"realTimeData": true,
"station": true,
"availableResources": 12,
"spacesAvailable": 11,
"allowDropoff": true,
"companyZoneId": 412,
"bikesAvailable": 12


"id": "382:1_8111",
"name": "ESTRELA (BASÍLICA)",
"x": -9.160065,
"y": 38.713627,
"scheduledArrival": 0,
"locationType": 0,
"companyZoneId": 382,
"lat": 38.713627,
"lon": -9.160065


"id": "PT-LIS-A00130",
"name": "12VJ97",
"x": -9.146358,
"y": 38.712482,
"licencePlate": "12VJ97",
"range": 58,
"batteryLevel": 77,
"helmets": 2,
"model": "Askoll",
"resourceImageId": "vehicle_gen_ecooltra",
"realTimeData": true,
"resourceType": "MOPED",
"companyZoneId": 473
*/