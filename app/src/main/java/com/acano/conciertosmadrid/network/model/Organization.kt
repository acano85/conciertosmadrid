package com.acano.conciertosmadrid.network.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable
@Parcelize
data class Organization(
    @SerializedName("organization-desc")
    var organization_desc:String,
    @SerializedName("organization-name")
    var organization_name:String

): Parcelable