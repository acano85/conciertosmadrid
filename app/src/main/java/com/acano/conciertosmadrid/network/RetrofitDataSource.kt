package com.acano.conciertosmadrid.network

import com.acano.conciertosmadrid.data.RemoteDateSource
import com.acano.conciertosmadrid.network.PlaceDb.service
import com.acano.conciertosmadrid.network.model.Concerts
import com.acano.conciertosmadrid.network.model.Place


class RetrofitDataSource : RemoteDateSource {

    override suspend fun getPlaces(): List<com.acano.conciertosmadrid.domain.Place> =
        toDomainPlace(service.getConcerts().execute().body()!!.places)


    private fun toDomainPlace(result: List<Place>): List<com.acano.conciertosmadrid.domain.Place> {
        return(result.map {com.acano.conciertosmadrid.domain.Place(uid=0,
            id=it.id,
            title = it.title,
            relation = it.relation,
            lat = it.Location?.latitude,
            lon = it.Location?.longitude)})
        }


}


