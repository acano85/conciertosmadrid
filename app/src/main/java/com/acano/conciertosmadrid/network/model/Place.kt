package com.acano.conciertosmadrid.network.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Place(
    @SerializedName("id")
    var id:String="",
    @SerializedName("title")
    var title:String="",
    @SerializedName("relation")
    var relation:String="",
    @SerializedName("postal-code")
    var postal_code:Int=0,
    @SerializedName("street-address")
    var street_address:String="",
    @SerializedName("location")
    var Location:Location?=null,
    @SerializedName("organization")
    var Organization:Organization?=null

):Parcelable




