package com.acano.conciertosmadrid.network.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable
@Parcelize
data class Location(
    @SerializedName("latitude")
    var latitude:Double,
    @SerializedName("longitude")
    var longitude:Double

): Parcelable