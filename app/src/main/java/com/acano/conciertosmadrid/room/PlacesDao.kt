package com.acano.conciertosmadrid.room

import androidx.room.*
import androidx.room.OnConflictStrategy.IGNORE

@Dao
interface PlacesDao {

    @Query ("SELECT * FROM PlaceBd")
    fun getStoreItems():List<PlaceBd>

    @Query ("SELECT COUNT(id) FROM PlaceBd")
    fun getStoreItemCount():Int

    @Insert (onConflict = IGNORE)
    fun insertStoreItems(myStoreItemList:List<PlaceBd>)

    @Update ()
    fun updateStoreItem(myStoreItem:PlaceBd)
}