package com.acano.conciertosmadrid.room

import com.acano.conciertosmadrid.data.LocalDataSource
import com.acano.conciertosmadrid.domain.Place as DomainPlace

class RoomDataSource(db:PlacesDatabase) :LocalDataSource{

    private val placesDao= db.placesDao()

    override suspend fun isEmpty(): Boolean = placesDao!!.getStoreItemCount() > 0

    override suspend fun savePlaces(places: List<DomainPlace>) {
       placesDao!!.insertStoreItems(DomainPlaceToRoomPlace(places))
    }

    override suspend fun getPlaces(): List<DomainPlace> {
        return RoomPlaceToDomainPlace(placesDao!!.getStoreItems())
    }

    fun DomainPlaceToRoomPlace(places:List<DomainPlace>):List<PlaceBd>{
        return (places.map { PlaceBd(
            uid = 0,
            id=it.id,
            title = it.title,
            relation = it.relation,
            lat = it.lat,
            lon = it.lon)
        })
    }
    fun RoomPlaceToDomainPlace(placesbd:List<PlaceBd>):List<DomainPlace>{
        return (placesbd.map { DomainPlace(
            uid = 0,
            id=it.id,
            title = it.title,
            relation = it.relation,
            lat = it.lat,
            lon = it.lon)
        })
    }
}