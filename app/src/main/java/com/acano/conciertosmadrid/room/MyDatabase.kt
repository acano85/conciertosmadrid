package com.acano.conciertosmadrid.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


@Database(entities = [PlaceBd::class], version = 2)
abstract class PlacesDatabase : RoomDatabase() {

    abstract fun placesDao(): PlacesDao
}