package com.acano.conciertosmadrid.room

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.acano.conciertosmadrid.network.model.Place

import kotlinx.android.parcel.Parcelize
@Entity(tableName = "PlaceBd")
data class PlaceBd(
    @PrimaryKey (autoGenerate = true)
    var uid:Int,
    var id:String,
    var title:String="",
    var relation:String="",
    var lat:Double?=0.0,
    var lon:Double?=0.0
)




