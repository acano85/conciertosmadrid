package com.acano.conciertosmadrid.di

import android.app.Application
import androidx.room.Room
import com.acano.conciertosmadrid.PlacesApp
import com.acano.conciertosmadrid.data.LocalDataSource
import com.acano.conciertosmadrid.data.RemoteDateSource
import com.acano.conciertosmadrid.location.SharedPrefsUtils
import com.acano.conciertosmadrid.location.local.LocationLocalSource
import com.acano.conciertosmadrid.location.remote.LocationRemoteSource
import com.acano.conciertosmadrid.network.RetrofitDataSource
import com.acano.conciertosmadrid.room.PlacesDatabase
import com.acano.conciertosmadrid.room.RoomDataSource
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {
    val mApp:Application = Application()
    @Provides
    @Singleton
    fun AppProvider(): Application {
        return mApp
    }

    @Provides
    fun databaseProvider(app: PlacesApp) = Room.databaseBuilder(
        app.applicationContext,
        PlacesDatabase::class.java,
        "places-bd"
    ).build()

    @Provides
    fun localDatasourceProvider(database: PlacesDatabase) : LocalDataSource = RoomDataSource(database)


    @Provides
    fun remoteDatasourceProvider() : RemoteDateSource = RetrofitDataSource()
    @Provides
    fun locationDataSource(app: PlacesApp) : LocationLocalSource = LocationLocalSource(SharedPrefsUtils(app.applicationContext))

    @Provides
    fun locationDataRemoteSource(app: PlacesApp) : LocationRemoteSource = LocationRemoteSource(SharedPrefsUtils(app.applicationContext))


}