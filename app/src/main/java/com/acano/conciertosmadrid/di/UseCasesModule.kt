package com.acano.conciertosmadrid.di

import com.acano.conciertosmadrid.data.PlacesRepository
import com.acano.conciertosmadrid.usecases.GetPlacesUseCase
import dagger.Module
import dagger.Provides
@Module
class UseCasesModule {
    @Provides
    fun getPlaces(placesRepository: PlacesRepository) =
        GetPlacesUseCase(placesRepository)
}