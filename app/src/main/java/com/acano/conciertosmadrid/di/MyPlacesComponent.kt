package com.acano.conciertosmadrid.di

import com.acano.conciertosmadrid.PlacesApp
import com.acano.conciertosmadrid.location.SharedPrefsUtils
import com.acano.conciertosmadrid.location.SharedPrefsUtilsModule
import com.acano.conciertosmadrid.ui.MainActivityComponent
import com.acano.conciertosmadrid.ui.MainActivityModule
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, DataModule::class, SharedPrefsUtilsModule::class] )
interface MyPlacesComponent {
    fun plus(module: MainActivityModule): MainActivityComponent

    @Component.Factory
    interface Factory{
        fun create(@BindsInstance app: PlacesApp): MyPlacesComponent
    }
}