package com.acano.conciertosmadrid.di

import android.app.Application
import android.content.Context
import com.acano.conciertosmadrid.PlacesApp
import com.acano.conciertosmadrid.data.LocalDataSource
import com.acano.conciertosmadrid.data.LocationRepository
import com.acano.conciertosmadrid.data.PlacesRepository
import com.acano.conciertosmadrid.data.RemoteDateSource
import com.acano.conciertosmadrid.location.SharedPrefsUtilsModule_ProvidesApplicationFactory.providesApplication
import com.acano.conciertosmadrid.location.local.LocationLocalSource
import com.acano.conciertosmadrid.location.remote.LocationRemoteSource
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataModule {

    @Provides
    @Singleton
    fun providesApplication(): Application = PlacesApp()

    @Provides
    @Singleton
    fun providesApplicationContext(): Context = providesApplication().applicationContext


    @Provides
    fun getPlaces(localPlacesRepository: LocalDataSource, remotePlacesRepository: RemoteDateSource)=
        PlacesRepository(
            localPlacesRepository,
            remotePlacesRepository
        )
    @Provides
    fun getLocation(locationLocalSource: LocationLocalSource, locationRemoteSource: LocationRemoteSource)=
        LocationRepository(
            locationLocalSource,
            locationRemoteSource
        )
}