package com.acano.conciertosmadrid.storeitem

import com.acano.conciertosmadrid.domain.Place
import com.acano.conciertosmadrid.room.PlaceBd
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem

data class StoreItem(
    val place: Place
) :
    ClusterItem {
    private var mPosition: LatLng = LatLng(place.lat!!, place.lon!! )
    override fun getSnippet(): String {
        return place.relation
    }

    override fun getTitle(): String {
        return place.title
    }

    override fun getPosition(): LatLng {
        return mPosition
    }
}