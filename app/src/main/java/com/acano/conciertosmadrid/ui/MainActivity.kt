package com.acano.conciertosmadrid.ui
import android.annotation.SuppressLint
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import com.acano.conciertosmadrid.R
import com.acano.conciertosmadrid.R.*
import com.acano.conciertosmadrid.mvvm.MainViewModel
import com.acano.conciertosmadrid.utils.app
import com.acano.conciertosmadrid.utils.getViewModel
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.activity_detail.*


class MainActivity : BaseActivity() {

    private lateinit var component: MainActivityComponent
    private val viewModel: MainViewModel by lazy { getViewModel { component.mainViewModel } }

    @SuppressLint("SetTextI18n", "SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component = app.component.plus(MainActivityModule())
        viewModel.model.observe(this, Observer { model: MainViewModel.UiModel -> updateUI(model) })
    }
    private fun updateUI(model: MainViewModel.UiModel){

        when(model){
            is MainViewModel.UiModel.getLocationInfo -> getLocationInfo(model.latLng)
            is MainViewModel.UiModel.getPlacesInfo -> getPlacesInfo(model.places)
            is MainViewModel.UiModel.showError -> showError(model.error)
        }
    }
    @SuppressLint("MissingPermission")
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
            viewModel.getLoc()
            super.onOptionsItemSelected(item)
        return true
        }

 }



