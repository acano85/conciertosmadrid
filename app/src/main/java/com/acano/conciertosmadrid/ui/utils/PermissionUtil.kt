package com.acano.conciertosmadrid.ui.utils

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import com.acano.conciertosmadrid.ui.BaseActivity

class PermissionUtil(val context: Context) {
    private lateinit var mActivity: BaseActivity

   fun onInit()
    {
         mActivity= context as BaseActivity
    }

    /**
     * Return the current state of the permissions needed.
     */
    fun checkPermissions(): Boolean {

        val permissionState = ActivityCompat.checkSelfPermission(
            mActivity,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
        return permissionState == PackageManager.PERMISSION_GRANTED
    }

    private fun startLocationPermissionRequest() {
        mActivity.let {
            ActivityCompat.requestPermissions(
                it,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION),
                BaseActivity.REQUEST_PERMISSIONS_REQUEST_CODE
            )
        }
    }

    fun requestPermissions() {
        startLocationPermissionRequest()
        if (ActivityCompat.checkSelfPermission(
                mActivity,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                mActivity,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
        }

    }

}