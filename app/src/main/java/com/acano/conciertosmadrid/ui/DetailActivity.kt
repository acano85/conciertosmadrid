package com.acano.conciertosmadrid.ui
import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.acano.conciertosmadrid.R.*
import kotlinx.android.synthetic.main.activity_detail.*


class DetailActivity : AppCompatActivity() {

    private var relation: String=""

    @SuppressLint("SetTextI18n", "SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout.activity_detail)
        val toolbar: Toolbar = findViewById<View>(id.toolbar) as Toolbar
        toolbar.title= "Datalle"
        setSupportActionBar(toolbar);
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        val intent = intent
        relation = intent.getStringExtra("relation")
        loadWebView(relation)
    }

    private fun loadWebView(url: String){
        myWebView.getSettings().setJavaScriptEnabled(false);
        myWebView.setFocusableInTouchMode(false);
        myWebView.setFocusable(false);
        val myWebViewClient=WebViewClient()
        myWebView.webViewClient=myWebViewClient
        myWebView.loadUrl(url)
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}

