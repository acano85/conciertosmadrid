package com.acano.conciertosmadrid.ui.utils

import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.os.StrictMode
import android.util.DisplayMetrics
import com.acano.conciertosmadrid.R
import com.acano.conciertosmadrid.storeitem.StoreItem
import com.acano.conciertosmadrid.ui.BaseActivity
import com.acano.conciertosmadrid.ui.DetailActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.maps.android.clustering.ClusterManager

class MapUtil(val context: Context): OnMapAndViewReadyListener.OnGlobalLayoutAndMapReadyListener,
    GoogleMap.OnInfoWindowClickListener
{
    private var mActivity: BaseActivity?=null
    private var gmap: GoogleMap?=null

    private lateinit var locationManager: LocationManager
    private lateinit var mClusterManager: ClusterManager<StoreItem>

   fun onStart(){
       locationManager =  context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    val policy =
        StrictMode.ThreadPolicy.Builder().permitAll().build()
    StrictMode.setThreadPolicy(policy)
     mActivity = context as BaseActivity
    val mapFragment = mActivity?.supportFragmentManager?.findFragmentById(R.id.map) as SupportMapFragment
    OnMapAndViewReadyListener(mapFragment, this)
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mapReady(googleMap)
    }

    override fun onInfoWindowClick(marker: Marker?) {
        infoWindowClick(marker)
    }
    fun mapReady(googleMap: GoogleMap?) {

        // return early if the map was not initialised properly
        gmap = googleMap ?: return


        with(gmap) {
            // Hide the zoom controls as the button panel will cover it.
            this!!.uiSettings!!.isZoomControlsEnabled = false

            gmap?.setOnInfoWindowClickListener(this@MapUtil)
            setUpClusterer()
        }
    }
    fun infoWindowClick(marker: Marker?) {
        val intent= Intent(context, DetailActivity::class.java)
        intent.putExtra("relation", marker?.snippet)

        context.startActivity(intent)
    }
    internal fun setUpClusterer() {
        mClusterManager = ClusterManager<StoreItem>(context, gmap)
        val metrics = DisplayMetrics()
        mActivity?.windowManager?.defaultDisplay?.getMetrics(metrics)
        gmap?.setOnCameraIdleListener(mClusterManager)
        gmap?.setOnMarkerClickListener(mClusterManager)
    }
    fun zoomToLocation(mLastLocation: LatLng) {
        when {
            mLastLocation!= null -> {
                gmap?.animateCamera(
                    CameraUpdateFactory.newLatLngZoom(
                            mLastLocation, 12.0f
                        )
                    )
               }
        }
    }
     fun addItems(places: List<StoreItem>) {
        mClusterManager.addItems(places)
    }

     fun removeAllItems() {
        mClusterManager.clearItems()
    }
}