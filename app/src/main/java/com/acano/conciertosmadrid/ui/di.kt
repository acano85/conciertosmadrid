package com.acano.conciertosmadrid.ui

import com.acano.conciertosmadrid.data.LocationRepository
import com.acano.conciertosmadrid.data.PlacesRepository
import com.acano.conciertosmadrid.mvvm.MainViewModel
import com.acano.conciertosmadrid.usecases.GetLocationUseCase
import com.acano.conciertosmadrid.usecases.GetPlacesUseCase
import dagger.Module
import dagger.Provides
import dagger.Subcomponent

@Module
class MainActivityModule {


    @Provides
    fun mainViewModelProvider(getPlacesUseCase: GetPlacesUseCase, getLocationUseCase: GetLocationUseCase) = MainViewModel(getPlacesUseCase, getLocationUseCase)

    @Provides
    fun getPlacesProvider(placesRepository: PlacesRepository) =
        GetPlacesUseCase(placesRepository)

    @Provides
    fun getLocationProvider(locationRepository: LocationRepository) =
        GetLocationUseCase(locationRepository)
}

    @Subcomponent(modules = [(MainActivityModule::class)])
    interface MainActivityComponent {
        val mainViewModel: MainViewModel
    }
