package com.acano.conciertosmadrid.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.acano.conciertosmadrid.R
import com.acano.conciertosmadrid.storeitem.StoreItem
import com.acano.conciertosmadrid.ui.utils.MapUtil
import com.acano.conciertosmadrid.ui.utils.PermissionUtil
import com.google.android.gms.maps.model.LatLng


open class BaseActivity : AppCompatActivity() {

    private lateinit var permissionUtils: PermissionUtil
    private var mapUtil: MapUtil?=null
    var cLocation: LatLng? = null

    companion object {

        val TAG = "LocationProvider"

        val REQUEST_PERMISSIONS_REQUEST_CODE = 34
    }

    /*********************************************
     * OVERRIDE METHODS
     *********************************************/


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        permissionUtils= PermissionUtil(this)
        mapUtil = MapUtil(context = this)
        permissionUtils.onInit()
        mapUtil?.onStart()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return true
    }
    @SuppressLint("MissingPermission")
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            R.id.action_location -> {
                if (!permissionUtils.checkPermissions()) {
                    permissionUtils.requestPermissions()
                }
                else{
                    cLocation?.let { mapUtil?.zoomToLocation(it) }
                }
            }
        }
        return true
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {
        Log.i(BaseActivity.TAG, "onRequestPermissionResult")
        if (requestCode == BaseActivity.REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.size <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(BaseActivity.TAG, "User interaction was cancelled.")
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                cLocation?.let { mapUtil?.zoomToLocation(it) }
            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        }
    }

    /*********************************************
     * INTERNAL METHODS
     *********************************************/


    internal fun getPlacesInfo(places: List<StoreItem>) {
        mapUtil?.addItems(places)
    }
    internal fun getLocationInfo(latLng: LatLng?) {
        if (latLng != null) {
            cLocation= latLng
            mapUtil?.zoomToLocation(cLocation!!)
        }
    }

    internal fun showError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

}



