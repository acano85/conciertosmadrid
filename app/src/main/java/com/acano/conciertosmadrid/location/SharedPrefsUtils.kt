package com.acano.conciertosmadrid.location

import android.content.Context

import android.content.SharedPreferences
import android.content.Context.MODE_PRIVATE
import com.acano.conciertosmadrid.PlacesApp
import com.google.android.gms.maps.model.LatLng
import android.content.Context.MODE_PRIVATE
import javax.inject.Inject


val LOCATION: String ="LOCATION"



val LAST_LAT: String ="lat"
val LAST_LON: String ="lon"



class SharedPrefsUtils(val context: Context) {


    val sharedPref = context.getSharedPreferences(LOCATION, Context.MODE_PRIVATE)


    fun getLocationFromPreferences(): LatLng? {
        val lat=sharedPref.getString(LAST_LAT,"lat")
        val lon=sharedPref.getString(LAST_LON,"lon")
        if(!lat!!.equals("lat") && !lon!!.equals("lon")){
            return LatLng(lat!!.toDouble(), lon!!.toDouble())
        }else return null
    }
fun getContextFromLocation():Context{
    return context
}
    fun saveLocation(location: LatLng?) {
        with(sharedPref.edit()) {
            putString(LAST_LAT, location?.latitude.toString()).commit()

            putString(LAST_LON, location?.longitude.toString()).commit()

        }
    }

}

