package com.acano.conciertosmadrid.location.local

import com.acano.conciertosmadrid.data.LocationRepository
import com.acano.conciertosmadrid.location.SharedPrefsUtils
import com.google.android.gms.maps.model.LatLng

class LocationLocalSource(var sharedPreferences: SharedPrefsUtils):
LocationRepository.PreferencesSource {


     override suspend fun saveLocation(location: LatLng) {
        sharedPreferences.saveLocation(location)
    }


    override suspend fun getLocation(): LatLng?{
           return sharedPreferences.getLocationFromPreferences()
    }



}