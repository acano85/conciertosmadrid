package com.acano.conciertosmadrid.location

import android.app.Application
import android.content.Context
import com.acano.conciertosmadrid.PlacesApp
import com.acano.conciertosmadrid.location.local.LocationLocalSource
import com.acano.conciertosmadrid.location.remote.LocationRemoteSource
import com.google.android.gms.maps.model.LatLng
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SharedPrefsUtilsModule {

    @Provides
    @Singleton
    fun providesApplication(): Application = PlacesApp()

    @Provides
    @Singleton
    fun providesApplicationContext(): Context = providesApplication().applicationContext


    @Provides
    suspend fun saveLocation(location: LatLng) = LocationLocalSource(sharedPreferences = SharedPrefsUtils(
        providesApplicationContext()
    )
    ).saveLocation(location)

    @Provides
    suspend fun getLocation():LatLng? = LocationLocalSource(SharedPrefsUtils(
        providesApplicationContext()
    )
    ).getLocation()
    @Provides
    suspend fun getLocationFromRemote():LatLng? = LocationRemoteSource(SharedPrefsUtils(
        providesApplicationContext()
    )
    ).getLocationFromRemote()
}
