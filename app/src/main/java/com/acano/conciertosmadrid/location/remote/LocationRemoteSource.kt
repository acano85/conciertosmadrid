package com.acano.conciertosmadrid.location.remote

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.location.LocationListener
import android.os.Bundle
import com.acano.conciertosmadrid.data.LocationRepository
import com.acano.conciertosmadrid.location.SharedPrefsUtils
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng

class LocationRemoteSource(var sharedPreferences: SharedPrefsUtils):
    LocationListener,
LocationRepository.PreferencesRemoteSource {
    var fusedLocationClient: FusedLocationProviderClient?= null

    override suspend fun getLocationFromRemote(): LatLng? {
        val context=sharedPreferences.getContextFromLocation()
        return getLoc(context)
    }

      @SuppressLint("MissingPermission")
      fun getLoc(context: Context):LatLng?{
          fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)

          var loc:LatLng?=null
            if(fusedLocationClient?.lastLocation !=null) {
                fusedLocationClient?.lastLocation!!
                    .addOnSuccessListener { location: Location? ->
                        if (location != null) {
                            loc=LatLng(location.latitude, location.longitude)
                            sharedPreferences.saveLocation(LatLng(location.latitude, location.longitude))
                        }
                    }
            }
      return loc
      }

    override fun onLocationChanged(p0: Location) {
        sharedPreferences.saveLocation(LatLng(p0.latitude, p0.longitude))
    }

    override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
        TODO("Not yet implemented")
    }

    override fun onProviderEnabled(p0: String?) {
        TODO("Not yet implemented")
    }

    override fun onProviderDisabled(p0: String?) {
        TODO("Not yet implemented")
    }


}