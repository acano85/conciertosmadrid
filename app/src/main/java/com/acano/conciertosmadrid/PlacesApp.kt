package com.acano.conciertosmadrid

import android.app.Application
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import com.acano.conciertosmadrid.di.DaggerMyPlacesComponent
import com.acano.conciertosmadrid.di.MyPlacesComponent
import com.acano.conciertosmadrid.utils.app

class PlacesApp: Application() {

    lateinit var component: MyPlacesComponent

    override fun onCreate() {
        component = DaggerMyPlacesComponent
            .factory()
            .create(this)
        super.onCreate()
    }
    public fun getApp():Application{
        return this
    }
}