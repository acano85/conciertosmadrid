package com.acano.conciertosmadrid


import com.acano.conciertosmadrid.data.LocalDataSource
import com.acano.conciertosmadrid.data.PlacesRepository
import com.acano.conciertosmadrid.data.RemoteDateSource
import com.acano.conciertosmadrid.domain.Place
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class PlacesRepositoryTest {

    @Mock
    lateinit var localDataSource: LocalDataSource

    @Mock
    lateinit var remoteDataSource: RemoteDateSource


    lateinit var placesRepository: PlacesRepository

    @Before
    fun setUp() {
        placesRepository =
            PlacesRepository(localDataSource, remoteDataSource)
    }

    @Test
    fun `getPlaces gets from local data source first`() {
        runBlocking {

            val localPlaces = listOf(mockedPlace.copy(1))
            whenever(localDataSource.getPlaces()).thenReturn(localPlaces)

            val result = placesRepository.getPlaces()

            assertEquals(localPlaces, result)
        }
    }

    @Test
    fun `getPlaces saves remote data to local`() {
        runBlocking {

            val remotePlace = listOf(mockedPlace.copy(2))
            whenever(remoteDataSource.getPlaces()).thenReturn(remotePlace)

            placesRepository.getPlaces()

            verify(localDataSource).savePlaces(remotePlace)
        }
    }




    private val mockedPlace = Place(
        0,
        "Title",
        "Overview",
        "relation"
    )
}